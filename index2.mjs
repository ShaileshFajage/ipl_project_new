
async function matchesWonPerTeam() {

    const result = await fetch("./data/matches.csv")


    const matchData = await result.text();


    let rows = matchData.split("\n").slice(1).map((row) => {

        return row;
    })

    rows = rows.slice(0, rows.length-1);

    const matchesWon = rows.reduce((matchesWon, eachData) => {


        const yearWiseData = eachData.split(',');

        let year = yearWiseData[1];

        let won = yearWiseData[10];

        if (won == "") {
            
        }
        else{

            if (matchesWon[won]) {
                if (matchesWon[won][year]) {
                    matchesWon[won][year] += 1
                }
                else {
                    matchesWon[won][year] = 1
                }
            }
            else {
                matchesWon[won] = {};
                matchesWon[won][year] = 1
            }

            
        }
        
        return matchesWon;

    }, {})

    console.log(matchesWon)

    return matchesWon

}

export { matchesWonPerTeam }


async function getEconomy() {

    const result = await fetch("./data/matches.csv")


    const matchData = await result.text();


    const matchRows = matchData.split("\n").slice(1).map((row) => {

        return row;
    })

    //console.log(matchRows)

    let match2015 = matchRows.filter((each) => {
        let val = each.split(",");
        if (val[1] == 2015) {
            return each;
        }
    })

    let firstId = match2015[0].split(",")[0];
    let lastId = match2015[match2015.length - 1].split(",")[0];

    //console.log(firstId, lastId)

    const fetchedData = await fetch("./data/deliveries.csv")


    const deliveriesData = await fetchedData.text();

    let delRows = deliveriesData.split("\n").slice(1).map((row) => {

        return row;
    })

    delRows = delRows.slice(0, delRows.length-1)



    let filteredDel = delRows.filter((each) => {

        let deldata = each.split(",");
        

        if (Number(deldata[0]) >= firstId && Number(deldata[0]) <= lastId) {
            
            return each;
        }
    })

    //console.log(filteredDel)

    // let result1 = filteredDel.reduce((result, each) => {

        // let val = each.split(",");
        // let key = val[8];

        // if (result[key]) {

        //     result[key] += Number(val[17]);
           

        // }
        // else {

        //     result[key] = Number(val[17]);
            

        // }

    //     //console.log(result)

    //     return result;

    // }, {})

    // console.log(result1)

    let output = filteredDel.reduce((result, each) => {

        let val = each.split(",");
        let key = val[8];

    

        if (result.hasOwnProperty(key)) {

            

            if(result[key]["total_runs"])
            {
                
                result[key]["total_runs"] += Number(val[17]) 
            }
            else
            {
                result[key]["total_runs"] = Number(val[17])
            }

            
            if(result[key].hasOwnProperty("total_over"))
            {
                if(Number(val[5])==6)
                {
                    result[key]["total_over"]+=1
                }
            }
            else
            {
                
                result[key]["total_over"] = 0 ;
            }
           

        }
        else {

            result[key] = {};
            result[key]["total_runs"] = 0;
            result[key]["total_over"] = 0;

        }

        return result;

    },{})

    console.log(output);

    let fresult = {};

    for(let key in output)
    {
        fresult[key] = output[key]["total_runs"] / output[key]["total_over"];
    }

    console.log(fresult)


    // let result2 = filteredDel.reduce((result, each) => {

    //     let val = each.split(",");
    //     let key = val[8];


    //     if (result.hasOwnProperty(key)) {
            

            // if(Number(val[5])==6)
            // {
            //     result[key]+=1
            // }


    //     }
    //     else {

    //         result[key] = 0;

    //     }

    //     return result;

    // }, {})

    // console.log(result2)


    // for (let property in result1) {
    //     result1[property] = result1[property] / result2[property];

    // }

    const sortable = Object.fromEntries(
        Object.entries(fresult).sort(([,a],[,b]) => a-b)
    );
    
    const finalResult = Object.keys(sortable).slice(0, 10).reduce((result, key) => {
        result[key] = sortable[key];
    
        return result;
    }, {});

    console.log(finalResult)

    return finalResult;
    

}


export { getEconomy }